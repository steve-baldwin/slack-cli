require "slack-api"
require "./ext/**"
require "./slack-cli/**"

module Slack::Cli
  VERSION = "0.1.0"
end
