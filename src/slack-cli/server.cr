require "http/server"
require "log"

module Slack::Cli
  SERVER_PORT = ENV["SERVER_PORT"]? || "3000"

  class Server
    @logger : Log?
    @server : HTTP::Server

    def initialize(engine : Engine, signing_secret : String? = nil)
      handlers = [HTTP::ErrorHandler.new] of HTTP::Handler
      engine.logger.try do |logger|
        @logger = logger
        handlers << LogHandler.new(logger)
      end
      handlers << SlackHandler.new(signing_secret)
      if db = engine.db
        handlers << DBHandler.new(db)
      end

      @server = HTTP::Server.new(handlers) do |context|
        context.engine = engine
        case context.endpoint
        when "event"
          if e = Slack::Event.from_body(context.body)
            if ret = e.process(context)
              context.log_info("Responding to %s with %s" % [e.type, ret])
              context.respond(ret)
            end
          else
            context.log_warn("Couldn't instantiate event from %s" % context.body)
          end
        when "action"
          context.local.clear
          if resp = engine.action_execute(context)
            context.respond(resp)
          end
        when "cmd"
          #
          # Run command
          #
          context.local.clear
          if resp = engine.command_execute(context)
            context.respond(resp)
          end
        else
          nil
        end
      end
      @server.bind_tcp("0.0.0.0", SERVER_PORT.to_i)
    end

    def log_warn(msg)
      @logger.try &.warn { msg }
    end

    def listen
      log_warn("Listening on port #{SERVER_PORT}")
      Signal::INT.trap do
        log_warn "Shutting down"
        @server.close unless @server.closed?
        exit
      end
      @server.listen
    end
  end
end
