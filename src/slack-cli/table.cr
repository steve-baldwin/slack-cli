module Slack::Cli
  private PUNCTUATION      = Set{'!', '.', '$', ',', '?', '%', ')', ']', '}', '*'}
  MAX_MESSAGE_SIZE = 16000
  MSG_TRUNC        = "[message truncated]"
  MSG_TRUNC_SZ     = MSG_TRUNC.size

  # Create a textural table of data that can be used in responses.
  class Table
    alias ColType = String | Int32 | Int64 | Float32 | Float64 | Time | Nil
    @data : Array(Array(String))
    @rjust : Array(Bool)
    @width : Array(Int32)
    @header : Bool
    @cols = -1
    @rows = 0

    # Create an instance of `Table`. If `@header` is true a heading row will be included in the output.
    def initialize(@header = true)
      @data = [] of Array(String)
      @rjust = [] of Bool
      @width = [] of Int32
      @max_width = [] of Int32
      @newlines = [] of Int32
      @row_end = [] of Bool
      @have_multi_line = false
    end

    def initialize(headings : Array(String))
      initialize(true)
      add_row(headings)
    end

    # Add a row of data. Column data is represented by each `String` of the `Array`
    def add_row(row : Array(ColType))
      if @cols == -1
        @cols = row.size
        if @max_width.size > 0 && @cols != @max_width.size
          raise "Expecting #{@max_width.size} columns, found #{row.size}"
        end
        @rjust = Array.new(row.size, false)
        @width = Array.new(row.size, 0)
      else
        if row.size != @cols
          raise "Expecting #{@cols} columns, found #{row.size}"
        end
      end
      #
      # Do we need to split any of the column values due to max width exceeded?
      #
      work = [] of String
      row.each_with_index do |val, i|
        lines = [] of String
        max = @max_width[i]? || Int32::MAX
        max = Int32::MAX if max == 0
        val = "" unless val
        val = val.to_s
        val.split('\n').each do |line|
          len = line.size
          while line.size > max
            #
            # Search backward from the max
            #
            state = 'u'
            bpos = 0
            (0...max).reverse_each do |p|
              c = line[p]
              case state
              when 'u'
                if c.ascii_letter?
                  state = 'w'
                elsif c.ascii_whitespace?
                  state = 's'
                elsif PUNCTUATION === c
                  #
                  # Look forward in case the next character is a space
                  #
                  if line[p + 1].ascii_whitespace?
                    bpos = p + 1
                    break
                  end
                end
              when 'w'
                if c.ascii_whitespace?
                  state = 's'
                end
              when 's'
                if c.ascii_letter?
                  bpos = p + 1
                  break
                elsif PUNCTUATION === c
                  bpos = p + 1
                  break
                end
              end
            end
            lines << line[0, bpos].rstrip
            line = line[bpos, len].lstrip
          end
          lines << line
        end
        work << lines.join('\n')
      end

      max_nl = 0
      work.each_with_index do |val, i|
        nl = val.count('\n')
        max_nl = nl if nl > max_nl
      end
      #
      # Do we need to potentially add newlines to column values to even
      # things up?
      #
      if max_nl == 0
        @data << work
        @row_end << true
      else
        @have_multi_line = true
        (0..max_nl).each do |i|
          @row_end << (i == max_nl)
          tmp_row = [] of String
          work.each do |val|
            tmp_row << (val + ("\n" * (max_nl - val.count('\n')))).split('\n')[i]
          end
          @data << tmp_row
        end
      end
      @newlines << max_nl
    end

    # Specify the maximum width of each column
    def max_width=(cols : Array(Int32))
      raise "You need to set max_width before adding any rows" unless @data.size == 0
      @max_width = cols
    end

    # Specify
    def set_rjust(rcols : Array(Int32))
      raise "You need to call add_row at least once before calling set_rjust" if @cols < 1
      rcols.each do |col|
        if col >= @cols
          raise "col must be between 0 and #{@cols - 1}."
        end
        @rjust[col] = true
      end
    end

    # Convert the table of data to a string
    def to_s(max_chars : Int32 = MAX_MESSAGE_SIZE)
      #
      # Work out the max size of each column
      #
      @data.each do |row|
        row.each_with_index do |val, cnum|
          w = val.size
          @width[cnum] = w if w > @width[cnum]
        end
      end
      #
      # Now build the table
      #
      rowsep = String.build do |b|
        b << "\n"
        @width.each_with_index do |w, i|
          b << "+" if i > 0
          b << "-" * (w + 2)
        end
      end
      String.build do |t|
        last : String? = nil
        t_size = 0
        @data.each_with_index do |row, rnum|
          line = String.build do |l|
            row.each_with_index do |val, cnum|
              last_col = (cnum == (@cols - 1))
              pad = ""
              sz = val.size
              max_sz = @width[cnum]
              #
              # Determine the space padding. We don't pad the final column
              # unless it is right justified.
              #
              if (sz < max_sz) && (!last_col || @rjust[cnum])
                pad = " ".rjust(max_sz - sz)
              end
              l << '|' if cnum > 0
              l << " "
              if @rjust[cnum]
                l << pad + val
              else
                l << val + pad
              end
              l << " " unless last_col
            end
            if @header && ((rnum == 0) || (@have_multi_line && @row_end[rnum]))
              l << rowsep
            end
            l << "\n"
          end
          if last
            if t_size + last.size + line.size > max_chars
              if t_size + last.size + MSG_TRUNC_SZ > max_chars
                t << MSG_TRUNC
              else
                t << last
                t << MSG_TRUNC
              end
              last = nil
              break
            end
            t << last
            t_size += last.size
          end
          last = line
        end
        if last
          t << last
        end
      end
    end
  end
end
