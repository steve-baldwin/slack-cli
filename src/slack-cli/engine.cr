require "db"
require "log"
require "uri"
require "http/client"
require "./processor"

module Slack::Cli
  class Engine
    @parse_error_msg : String
    property logger : Log?
    property db : DB::Database?
    setter processors = [] of Processor
    setter processing_msg : String
    setter sync_wait = 1.0
    setter before_action : Proc(HTTP::Server::Context, Nil)?
    setter after_action : Proc(HTTP::Server::Context, Nil)?
    setter before_command : Proc(HTTP::Server::Context, Nil)?
    setter after_command : Proc(HTTP::Server::Context, Nil)?
    setter save_history : Proc(HTTP::Server::Context, Nil)?
    setter save_ok : Bool = false

    def initialize(@parse_error_msg, @db : DB::Database? = nil, processing_msg : String? = nil)
      @processing_msg = processing_msg || "Thanks <@%s>. I'm working on your request and will get back to you shortly."
    end

    def action_execute(context : HTTP::Server::Context)
      payload = Slack::Cli::ActionPayload.from_json(context.params["payload"])
      match = {payload.callback_id, payload.actions[0].name}
      context.log_info("Processing action: %s" % match.to_s)
      @processors.each do |processor|
        processor.action_ids.each do |action_id|
          processing_msg = @processing_msg
          next unless match == {action_id[0], action_id[1]}
          if action_id.is_a? Tuple(String, String, String)
            processing_msg = action_id[2]
          end
          context.params["channel_id"] = payload.channel.id
          context.params["user_id"] = payload.user.id
          if cb = @before_action
            cb.call(context)
          end
          ret = action_process(processor, payload, context, match, processing_msg)
          if cb = @after_action
            cb.call(context)
          end
          ret
        end
      end
      context.log_warn("No registered action handler for %s" % match.to_s)
      nil
    end

    def command_execute(context : HTTP::Server::Context)
      @save_ok = true
      context.log_info("Processing command: %s" % (context.tokens.map { |x| "[%s]" % x }.join(", ")))
      if cb = @before_command
        cb.call(context)
      end
      ret = command_parse(context)
      if cb = @after_command
        cb.call(context)
      end
      if @save_ok && (cb = @save_history)
        cb.call(context)
      end
      ret
    end

    def do_process(context, user_id, processing_msg, response_url, &block)
      done = false
      msg = processing_msg || @processing_msg
      #
      # Work out how long we should sleep before delivering the processing message.
      # Factor in how long it has been since the request was received.
      #
      sleep_sec = @sync_wait - (Time.utc - context.t_start).total_seconds
      spawn do
        sleep sleep_sec if sleep_sec > 0
        unless done
          done = true
          context.respond(msg % user_id)
        end
      end
      resp = yield
      already_responded = done
      done = true
      return nil unless resp
      #
      # Convert a plain text response into a response object
      #
      if already_responded
        context.log_info("delivering async content")
        uri = URI.parse(response_url)
        HTTP::Client.new(uri) do |client|
          if p = uri.path
            case resp
            when String
              resp = Slack::Cli::Response.new(resp)
            end
            headers = HTTP::Headers.new
            headers["Content-Type"] = "application/json"
            r = client.post(p, headers, resp.to_json.to_s)
          end
        end
        return nil
      else
        case resp
        when Slack::Cli::Response
          context.response.headers["Content-Type"] = "application/json"
          resp = resp.to_json.to_s
        end
        return resp
      end
    end

    def action_process(processor, payload, context, match, processing_msg) : String?
      do_process(context, payload.user.id, processing_msg, payload.response_url) { processor.process_action(context, payload, match) }
    end

    def command_process(processor, context, name, processing_msg) : String?
      do_process(context, context.params["user_id"], processing_msg, context.params["response_url"]) { processor.process_command(context, name) }
    end

    def command_parse(context, from_history : Bool = false)
      tokens = context.tokens
      ts = tokens.size
      @processors.each do |processor|
        next unless processor.is_valid?(context)
        processor.rulesets.each do |rs|
          next if rs.rules.size != ts
          match = true
          rs.rules.zip(tokens) do |rule, token|
            case rule
            when String
              unless rule == token
                match = false
                break
              end
            when Regex
              unless rule.match(token)
                match = false
                break
              end
            when Token
              unless rule.match(context, token)
                match = false
                break
              end
            end
          end
          if match
            if from_history
              resp = processor.process_command(context, rs.name)
              case resp
              when Slack::Cli::Response
                context.response.headers["Content-Type"] = "application/json"
                resp = resp.to_json.to_s
              end
              return resp
            else
              return command_process(processor, context, rs.name, rs.processing_msg)
            end
          end
        end
      end
      @save_ok = false
      return @parse_error_msg % context.command
    end

    def process_from_history(context, command : String)
      context.log_info("Executing from history: [%s]" % command)
      context.load_tokens_from_history(command)
      @save_ok = false
      command_parse(context, from_history: true)
    end
  end
end
