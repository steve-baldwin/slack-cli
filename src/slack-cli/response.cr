require "json"
require "slack-api"

module Slack::Cli
  class Response
    include JSON::Serializable
    property response_type = "ephemeral"
    property replace_original = true
    property text : String
    property attachments : Array(Slack::Request::ParamsAttachment)?

    def initialize(@text, @attachments = nil)
    end
  end
end
