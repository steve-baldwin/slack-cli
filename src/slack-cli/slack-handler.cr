require "http/server"
require "openssl/**"

module Slack::Cli
  private class BadRequest < ::Exception
  end

  private class SlackSignCheck < ::Exception
  end

  private class SlackHandler
    SLACK_SIG_VERSION   = "v0"
    MAX_CLOCK_DRIFT_SEC = 60
    ENDPOINT_RX         = /^\/slack\/(?<endpoint>cmd|action|event)\/?$/
    include HTTP::Handler

    @signing_secret : String? = nil

    def initialize(@signing_secret)
      unless @signing_secret
        @signing_secret = ENV["SLACK_SIGNING_SECRET"]? || "dilroy was here"
      end
    end

    def call(context)
      r = context.request
      #
      # Bail if not POST
      #
      unless r.method == "POST"
        raise BadRequest.new("Wrong http verb")
      end
      #
      # Bail unless /slack/{cmd|action|event}
      #
      m = ENDPOINT_RX.match(r.resource)
      raise BadRequest.new("Unexpected resource") unless m
      #
      # Slurp in the body
      #
      io = context.request.body.not_nil!
      body = io.gets_to_end
      #
      # Unless disabled, check the request is signed by Slack
      #
      unless ENV.has_key?("NO_AUTH_SLACK")
        unless ts = r.headers["X-Slack-Request-Timestamp"]?
          raise BadRequest.new("Missing X-Slack-Request-Timestamp header")
        end
        if drift = (Time.utc.to_unix - ts.to_i).abs > MAX_CLOCK_DRIFT_SEC
          raise SlackSignCheck.new("Clock drift exceeded (#{drift})")
        end
        header_sig = r.headers["X-Slack-Signature"]
        base_string = SLACK_SIG_VERSION + ":" + ts + ":" + body
        calc_sig = SLACK_SIG_VERSION + "=" + OpenSSL::HMAC.hexdigest(:sha256, @signing_secret.not_nil!, base_string)
        if header_sig != calc_sig
          raise SlackSignCheck.new("Invalid Slack signature")
        end
      end
      #
      # Parse the body depending on the Content-Type header
      #
      context.process_body(body)

      endpoint = m["endpoint"]
      if endpoint
        context.endpoint = m["endpoint"]
      end
      return call_next(context)
    rescue e : BadRequest
      context.response.status_code = 400
      context.log_error(e.message || "Hmmm")
    rescue e : SlackSignCheck
      context.response.status_code = 400
      context.log_error(e.message || "Splat")
    end
  end
end
