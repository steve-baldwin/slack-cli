require "db"
require "http/server"

module Slack::Cli
  private class DBHandler
    include HTTP::Handler

    def initialize(@db : DB::Database)
    end

    def call(context)
      context.log_info("Starting transaction")
      @db.transaction do |trx|
        context.db = trx.connection
        call_next(context)
      end
      context.log_info("Finished transaction")
      context
    end
  end
end
