module Slack::Cli
  class ActionPayloadAction
    include JSON::Serializable
    property name : String
    property type : String
    property value : String
  end

  class ActionPayloadTeam
    include JSON::Serializable
    property id : String
    property domain : String
  end

  class ActionPayloadChannel
    include JSON::Serializable
    property id : String
    property name : String
  end

  class ActionPayloadUser
    include JSON::Serializable
    property id : String
    property name : String
  end

  class ActionPayload
    include JSON::Serializable
    property type : String
    property actions : Array(ActionPayloadAction)
    property callback_id : String
    property team : ActionPayloadTeam
    property channel : ActionPayloadChannel
    property user : ActionPayloadUser
    property action_ts : String
    property message_ts : String
    property attachment_id : String
    property token : String
    property is_app_unfurl : Bool
    property response_url : String
    property trigger_id : String
  end
end
