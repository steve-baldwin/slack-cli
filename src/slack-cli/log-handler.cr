require "http/server"
require "log"

module Slack::Cli
  private class LogHandler
    include HTTP::Handler

    def initialize(@logger : Log)
    end

    def call(context)
      context.start_request(@logger)
      begin
        call_next(context)
        context.end_request
      rescue e : ::Exception
        context.log_error(e.inspect_with_backtrace)
        raise e
      end
      context
    end
  end
end
