module Slack::Cli
  alias RuleType = Token | String | Regex

  class RuleSet
    property name : String
    property rules : Array(RuleType)
    property processing_msg : String?

    def initialize(@name, @rules, @processing_msg = nil)
    end
  end
end
