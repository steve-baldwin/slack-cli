require "./ruleset"

module Slack::Cli
  abstract class Processor
    alias ActionId = Tuple(String, String) | Tuple(String, String, String)

    abstract def rulesets : Array(RuleSet)
    abstract def process_command(context : HTTP::Server::Context, name : String) : (String | Slack::Cli::Response)

    def action_ids : Array(ActionId)
      [] of ActionId
    end

    def process_action(context : HTTP::Server::Context, action : Slack::Cli::ActionPayload, match : ActionId) : String?
      raise "Unimplemented process_action method for %s" % self.class
    end

    def is_valid?(context : HTTP::Server::Context) : Bool
      true
    end
  end
end
