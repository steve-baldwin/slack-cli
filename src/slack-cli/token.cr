module Slack::Cli
  abstract class Token
    abstract def match(context : HTTP::Server::Context, val : String) : Bool

    def db(context : HTTP::Server::Context)
      context.db.not_nil!
    end
  end
end
