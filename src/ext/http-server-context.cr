require "log"
require "db"

class HTTP::Server
  enum ParseMode
    Space
    Quoted
    Word
    Regex
  end

  class Context
    @request_id = 0
    @t_start = Time.utc
    @logger : ::Log?
    @endpoint = ""
    @body = ""
    @params = Hash(String, String).new
    @tokens = [] of String
    @command = ""
    @db : DB::Connection?
    @engine : Slack::Cli::Engine?
    property local = Hash(String, String).new

    def start_request(logger : ::Log)
      @t_start = Time.utc
      @logger = logger
      @request_id = Random.new.rand(100000)
      logger.warn { "#{@request_id}|#{self.request.method} #{self.request.resource} start" }
    end

    def log_info(msg : String)
      @logger.try &.info { msg }
    end

    def log_warn(msg : String)
      @logger.try &.warn { msg }
    end

    def log_error(msg : String)
      @logger.try &.error { msg }
      response.print(msg)
      response.flush
    end

    def respond(content : String)
      response.status_code = 200
      response.print(content)
      response.close
    end

    def end_request
      ms = (Time.utc - @t_start).total_milliseconds.round(3)
      log_warn("#{self.request.method} #{self.request.resource} completed (#{self.response.status_code}) in #{ms}ms")
    end

    def engine=(val : Slack::Cli::Engine)
      @engine = val
    end

    def engine
      @engine.not_nil!
    end

    def endpoint=(val : String)
      @endpoint = val
    end

    def endpoint
      @endpoint
    end

    def command
      @command
    end

    def t_start
      @t_start
    end

    def get_tokens(text : String) : Array(String)
      state : ParseMode = ParseMode::Space
      quote : Char = ' '
      tokens : Array(String) = [] of String
      token : Array(Char) = [] of Char
      text_chars = text.chars
      text_chars << ' '
      text_chars.each do |c|
        case state
        when .space?
          unless c.ascii_whitespace?
            if c.in_set? %q{'"}
              state = ParseMode::Quoted
              quote = c
            elsif c == '/'
              state = ParseMode::Regex
              token << c
            else
              state = ParseMode::Word
              token << c
            end
          end
        when .quoted?
          if c == quote
            state = ParseMode::Space
            tokens << token.join
            token.clear
          else
            token << c
          end
        when .regex?
          token << c
          if c == '/'
            state = ParseMode::Word
          end
        when .word?
          if c.ascii_whitespace?
            state = ParseMode::Space
            tokens << token.join
            token.clear
          else
            token << c
          end
        end
      end
      tokens
    end

    def body
      @body
    end

    def process_body(body : String)
      if request.headers["Content-Type"] == "application/x-www-form-urlencoded"
        @params = HTTP::Params.parse(body).fetch
        #
        # Split the 'text' param into tokens
        #
        if t = @params["text"]?
          @tokens = get_tokens(t)
          @command = t
        end
      else
        @body = body
      end
    end

    def load_tokens_from_history(hist : String)
      @tokens = get_tokens(hist)
      @command = hist
    end

    def params
      @params
    end

    def tokens
      @tokens
    end

    def db
      @db.not_nil!
    end

    def db=(db)
      @db = db
    end
  end
end
